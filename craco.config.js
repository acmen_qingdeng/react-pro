
const path = require('path')
const pathResolve = pathUrl => path.join(__dirname, pathUrl);
module.exports = {
  webpack: {
    alias: {
      '@': pathResolve('src'),
      '@api': pathResolve('src/api'),
      '@assets': pathResolve('src/assets'),
      '@views': pathResolve('src/views'),
      '@router': pathResolve('src/router')
    },
  },
    babel: {
      plugins: [
          [
              "@babel/plugin-proposal-decorators",
              { "legacy": true }
          ]
      ]
    },

    devServer: {
      proxy: {
        "/api": {
          target: '',
          changeOrigin: true,
          pathRewrite: {
            "^/api": ""
          }
        }
      }
    }

  
};