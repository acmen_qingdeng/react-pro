import React from 'react';
import { connect } from 'react-redux';
import './table.scss'
import { addTodo } from '../../redux/actions/todo';


class ContentEnter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      contentStr: '',
    }
  }

  render() {
    const domELe = (
      <div className="addFormWrapper">
        <div className="addedTitle">
          添加备忘
        </div>
        <form id="addTodoForm" onSubmit={(e) => this.handleFormSubmit(e)}>
          <div className="inputItem">
            <input style={{color: '#666'}} type="text" name="contentStr" value={this.state.contentStr} onChange={ (e) => this.handleInputChange(e)} />
          </div>
          <div className="inputItem btn-box">
            <button type="submit" className="addedBtn">添加</button>
          </div>
        </form>
      </div> 
    )

    return domELe
  }

  handleFormSubmit(event) {
    event.preventDefault();
    console.log('表单数据', this.props)
    const { contentStr } = this.state;
    const { addTodo } = this.props;
    if(contentStr) {
      addTodo(contentStr);
      this.setState({
        contentStr: ''
      })
    }
  }

  handleInputChange(event) {
    const { name, value } = event.target;
    this.setState(
      {
         [name]: value
      }
    )
  }
}

function mapStateToProps(store) {
  return { todos: store.todos }
}

export default connect(mapStateToProps, {addTodo}) (ContentEnter);