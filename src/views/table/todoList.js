import React from 'react';
import { connect } from 'react-redux';
import './table.scss'
import { toggleTodo, delTodo } from '../../redux/actions/todo';

class TodoList extends React.Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    const { todos } = this.props;
    let page;
    if (todos && todos.length) {
      page = todos.map(item => {
        return (
          <div key={item.id} className="listItem">
            <div className="left">
              事项：{item.content}
            </div>
            <div className="right">
              <div className={'action ' + (item.completed ? 'suc' : 'err')} onClick={() => this.todoToggle(item)}>
                {item.completed ? '已完成' : '未完成'}
              </div>
              <div className="delBtn err noSelect" onClick={() => this.handleDelete(item)}>
                删除
              </div>
            </div>
          </div>
        )
      })
    } else {
      page = (<div className="empty dashBorder">暂时没有数据。。。</div>)
    }
    const domELe = (
      <div className="listWrapper">
        <div className="listTitle zz-title-1">
          备忘录
        </div>
        <div className="listContent">
          { page }
        </div>
      </div>
    )
    return domELe;
  }

  handleDelete(row) {
    console.log('row', row)
    const { delTodo } = this.props;
    delTodo(row.id)
  }

  
  todoToggle(row) {
    console.log('row', row);
    let { toggleTodo } = this.props;
    toggleTodo(row.id)
  }

}

const mapStateToProps = (store) => {
  return { todos: store.todos }
}

const mapDispatchToProps = { toggleTodo, delTodo }

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);