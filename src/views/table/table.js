import React from 'react';
import { connect } from 'react-redux';
import './table.scss';
import * as todoActions from '../../redux/actions/todo';
import ContentEnter from './addTodo';
import TodoList from './todoList';
import { Rate } from 'antd';

class Table extends React.Component  {
  constructor(props) {
    super(props);
    this.state = {
      title: '测试table'
    }
  }

  render() {
    // eslint-disable-next-line
    const  { todos }=this.props;
    const domEle = (
        <div className="bg">
          <div className="tableWrapper">
            <div className="pageTitle"> 
              { this.state.title }
            </div>
           </div>
          <TodoList></TodoList>
          <ContentEnter></ContentEnter>
          <div>
            <Rate allowHalf ></Rate>
          </div>
        </div>
    )
    return domEle
  }



}

const mapStateToProps = (store) => {
  console.log('store', store)
  return { todos: store.todos}
}

export default  connect(mapStateToProps, todoActions)(Table);