import React from 'react';
import { Button } from 'antd';
import './index.scss';
import PageWrapper from '@/components/layout/wrapper';

class Zcom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '张三',
    }
    console.log("# this is in component's constructor function")
  }

  static getDerivedStateFromProps(props, state) {
    console.log('# @ this is a function to init state from props, it will act every time to render')
    return {
      newState: 'zzz',
      ...state
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('# shouldComponentUpdate')
    return true
  }
  handleBtnActive() {
    console.log('changeState to active update')
    this.setState({
      name: '小四'
    })
  }

  render() {
    console.log('# @ well, component id rendering')
    console.log('this', this)
    const domEle = (
      <PageWrapper >
        <div className="chartWrapper">
          <h1>chart wrapper for test demo</h1>
          <p>{this.state.name}</p>
          <Button type="primary" size="large" onClick={() => this.handleBtnActive()}>修改出发update</Button>
        </div>
      </PageWrapper>
    )

    return domEle;
  }

  componentDidMount() {
    console.log('# this is in componentDidMount')
  }

  getSnapshotBeforeUpdate(preProps, preState) {
    console.log('@ getSnapshotBeforeUpdate')
    return {
      age: 12
    }
  }

  componentDidUpdate(props, state, snapData) {
    console.log('@ componentDidUpdate props', props)
    console.log('@ componentDidUpdate state', state)
    console.log('@ componentDidUpdate snapData', snapData)
  }

  componentWillUnmount() {
    console.log('* componentQWillUnmount')
  }

  componentDidCatch(error, info) {
    console.log('^^^^ componentDidCatch ^^^^')
  }
}


export default Zcom;