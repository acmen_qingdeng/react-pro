import React from 'react';


class Child extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      getData: this.props.inputs
    }

    console.log('this.props.inputs', this.props.inputs)
  }


  render() {

    const element = (
      <div className="inputsShow">
        {this.props.inputs}
      </div>
    )

    return element;
  }
}

export default Child