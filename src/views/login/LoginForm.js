import React from 'react';
import './Login.css';
import Child from './Child';

class LoginForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('提交数据:' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label className="formLabel">
          用户名：
          <input className="input" type="text" name="userName" id="userName" onChange={this.handleChange} />
        </label>
        <Child inputs={this.state.value} />
        <div className="submitBox" >
          <input type="submit"  value="提交" />
        </div>
      </form>
    )
  }
}


export default LoginForm;
