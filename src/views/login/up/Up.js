import React from 'react';
import '../varible.scss'
import './Up.scss'
/**
 * 数据流：通过props向下传递；通过事件向上传递
 */

class Input extends React.Component {

  constructor(props) {
    super(props);
    this.handleInputchange = this.handleInputchange.bind(this);
  }

  handleInputchange(event) {
    console.log('event', event)
    this.props.emitInputChange(event.target.value)
  }

  render() {
    const element = (
      <div className="inputItem">
        <input type="text" name="inputs" onChange={this.handleInputchange} value={this.props.datas} />
      </div>
    )

    return element

  }

}

class Show extends React.Component {

  render() {
    return (
      <div className="show">
        Show现在输入的内容是{this.props.ins}##
      </div>
    )
  }

}

class Up extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      mainValue: ''
    }

    this.handleChanges = this.handleChanges.bind(this)
  }

  handleChanges(e) {
    this.setState({
      mainValue: e
    })
  }

  render() {
   
    const element = (
      <div>
        <Input datas={this.state.mainValue} emitInputChange={this.handleChanges} />
        <div className="upShow">
          Up内容：{ this.state.mainValue }
        </div>
        <Show ins={ this.state.mainValue } />
      </div>
    )

    return element;
  }
}

export default Up;