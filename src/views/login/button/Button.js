import React from 'react';
import '../commen.css'

class Button extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isBtnClick: true };

    // 在回调中使用this， 所以必须绑定bind this；否则会报错 property setState undeifned
    // this.handleBtnClick = this.handleBtnClick.bind(this);
  }

  handleBtnClick(val) {
    console.log('val', val)
    this.setState(state => ({
      isBtnClick: !state.isBtnClick
    }))
  }

  render() {

    const element = (
      <button className="yioksBtn" onClick={this.handleBtnClick.bind(this, 'hellow')}>
        { this.state.isBtnClick ? 'ON' : 'OFF'}
      </button>
    )

    return element
  }
}

export default Button