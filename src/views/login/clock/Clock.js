import React from 'react';
import './Clock.css'

/**
 * 注意： 不可以直接修改this.state.xxx的值，不会更新状态
 * 由于，this.props和this.state的更新存在异步更新的情况
 * 所以不能依赖他们的状态来更新新的状态：
 * this.setState({
 *  counter: this.state.counter + this.props.increment;
 * })
 * // 以上更新状态的方法是不合法的；
 * 如果想解决这个问题，可以是setState接受一个函数，而不是一个对象；
 * 当setState接收一个函数更新状态时，函数将state作为第一个参数
 * 将props作为第二个参数；（这里的函数使用箭头函数和普通函数都
 * 可以（不涉及this的调用））
 * this.setState( (state, props)=> {
 *    return {
 *      counter； state.counter + props.increment
 *    }
 * })
 * 
 * 另外：调用setState()更新数据的时候，React会将更新的对象
 * 合并到当前的state中，这是一种浅合并，可以分别调用setState()
 * 来更新部分数据；
 * 
 * 数据流向：
 * React中，成state是局部的，封装的，其他组件是不能访问到
 * 其他组件的状态的；
 * 组件可以将自己的state作为props向下传递到他的子组件中
 */
class Clock extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      timeData : new Date().toLocaleDateString()
    }
  }

  // 更新时间
  updateTime() {
    setTimeout(() => {
     this.setState({ timeData : new Date().toLocaleTimeString()})
    },1000)
  }

  // 组件声明周期方法  = mounted
  componentDidMount() {
    console.log('this is the react life circle named componentDidMount');
    this.timeId = setInterval(() => {
      this.setState({ timeData : new Date().toLocaleTimeString()})
    }, 1000);
  }

  // 组件生命周期方法  === unmount
  componentWillUnmount() {
    console.log('this is the react life circle named componentWillUnmount');
    clearInterval(this.timeId);
  }

  render() {
    // this.updateTime();
    const clockElement = (
      <div className="clockBox">
        <span>{ this.state.timeData }</span>
      </div>
    )
    return clockElement;
  }

}

export default Clock;