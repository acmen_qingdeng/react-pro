import React from 'react';
import './List.scss'

class List extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      list: props.list
    }
  }

  render() {
    const listElement = (
      <div className="listWrap">
        <div className="listTitle">
          列表渲染
        </div>
        {
          this.state.list.map((item,index) => {
            return (
              <div className="listItem" key={item.id} data-key={item.key}>
                { index + 1 }:{ item.value }
              </div>
            )
          })
        }
      </div>
    )
    return listElement;
  }
}

export default List;