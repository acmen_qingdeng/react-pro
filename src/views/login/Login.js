import React from 'react';
import './Login.css';
// import LoginForm from './LoginForm';
// import Clock from './clock/Clock';
// import Button from './button/Button';
// import List from './list/List';
import Form from './forms/Form';
// import Up from './up/Up';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myList: [
        {
          id:1,
          key: 'key-1',
          value: 'key-1',
        },
        {
          id:2,
          key: 'key-2',
          value: 'key-2',
        },
        {
          id:3,
          key: 'key-3',
          value: 'key-3',
        },
      ],
      userNameFromHome: this.props.location.state
    }
  }
  render() {
    // console.log('state :', this.props.location.state.name)
    return (
      <div className="bg">
        {/* <Clock /> */}
        {/* <Button /> */}
        {/* <LoginForm /> */}
        {/* <List list={this.state.myList} /> */}
        <Form />
        {/* <Up /> */}
      </div>
    )
  }
}

export default Login;