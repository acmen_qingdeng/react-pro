import React from 'react';
import { Button } from 'antd';
import { connect } from 'react-redux';
import './Form.scss'
import { withRouter } from 'react-router';
import { setUserInfo, clearUserInfo } from '@/redux/actions/userInfo';

@withRouter
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        idName: '',
        idNumber: '',
        email: '',
        intro: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
  }

  handleInputChange(event) {
    /* 受控组件 */
    const { name, value } = event.target;
    
    this.setState(
      {
        [name]: value
      }
    )
  }

  handleSubmitForm(event) {
    console.log('表单数据', this.state);
    event.preventDefault();
    const { idName, idNumber } = this.state;
    if( idName && idNumber ) {
      console.log('push', this)
      this.props.storageUserInfo(this.state)
      this.props.history.push('/home/line')
    }
  }

  render() {

    const formElement = (
      <div className="formWrap">
        <form onSubmit={this.handleSubmitForm}>
          <div className="formItem">
            <label htmlFor="idName">用户名：</label>
            <input type="text" name="idName" onChange={this.handleInputChange} value={this.state.idName} />
          </div>
          <div className="formItem">
            <label htmlFor="idNumber">证件号：</label>
            <input type="text" name="idNumber" onChange={this.handleInputChange} value={this.state.idNumber} />
          </div>
          <div className="formItem">
            <label htmlFor="email">邮件地址：</label>
            <input type="text" name="email" onChange={this.handleInputChange} value={this.state.email} />
          </div>
          <div className="formItem">
            <label htmlFor="intro">备注：</label>
            <input type="text" name="intro" onChange={this.handleInputChange} value={this.state.intro} />
          </div>
          <div className="formItem btns-group">
            {/* <button type="submit">提交</button> */}
            <Button type="primary" htmlType="submit">提交</Button>
          </div>
        </form>
      </div>
    )

    return formElement
  }
}

const mapStateToProps = (state) => {
  return state.userInfo;
}

const mapDispatchToProps = (dispatch) => {
  return {
    storageUserInfo: (info) => {
      dispatch(setUserInfo(info));
    },

    removeUserInfo: () => {
      dispatch(clearUserInfo());
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Form);