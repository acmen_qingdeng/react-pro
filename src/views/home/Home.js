import React from 'react';
import { Space } from 'antd';
import { Line } from '@ant-design/charts';
import PageWrapper from '@/components/layout/wrapper';
import './Home.scss';


class HomePage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      chartConfig: {
        data: [
          { year: '1991', value: 3 },
          { year: '1992', value: 4 },
          { year: '1993', value: 3.5 },
          { year: '1994', value: 5 },
          { year: '1995', value: 4.9 },
          { year: '1996', value: 6 },
          { year: '1997', value: 7 },
          { year: '1998', value: 9 },
          { year: '1999', value: 13 },
        ],
        // autoFit: true,
        width: 580,
        xField: 'year',
        yField: 'value',
        color: {
          colorField: 'type',
          color: ['#55E6C1', '#F97F51', '#B33771']
        },
        lineStyle: {
          // fill: '#55E6C1',
          stroke: '#F97F51',
        },
        yAxis: {
          line: { style: '#F97F51' },
          label: {
            style: {
              stroke: '#000',  // 设置y轴字体颜色
            }
          },
          grid: {
            line: {
              style: {
                stroke: '#fff'
              }
            }
          },
        },
        xAxis: {
          line: { style: '#000' },
          label: {
            style: {
              stroke: '#000',
            }
          },

        },

        point: {
          size: 5,
          shape: 'diamond',
          color: '#B33771',
        },
      }
    }
  }

  render() {
    const element = (
      <PageWrapper>
        <div className="pageContentWrapper">
          <div className="line">
            <Space size="large" className="w100">
              <div className="lineItem">
                <div className="pannelWrapper">
                  <Line {...this.state.chartConfig}></Line>
                </div>
              </div>
              <div className="lineItem">
                <div className="pannelWrapper">
                  <Line {...this.state.chartConfig}></Line>
                </div>
              </div>
            </Space>
          </div>

        </div>
      </PageWrapper>
    )
    return element;
  }
}

export default HomePage