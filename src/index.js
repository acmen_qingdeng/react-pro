import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './assets/css/variable.scss';
import './index.css';

import reportWebVitals from './reportWebVitals';

import { Provider } from 'react-redux'
import reducers from './redux/reducers'
import { createStore } from 'redux';
import zhCN from 'antd/lib/locale/zh_CN';
import { ConfigProvider } from 'antd';

import RouterView from './router';  // 加载路由视图

let store = createStore(reducers)
/* 严格模式下，antd会报warning
<React.StrictMode>
    <ConfigProvider locale={zhCN}>
      <Provider store={store}>
        <RouterView></RouterView>
      </Provider>
    </ConfigProvider> 
  </React.StrictMode>
 */
// <HomeRouter />
ReactDOM.render(
    <ConfigProvider locale={zhCN}>
      <Provider store={store}>
        <RouterView></RouterView>
      </Provider>
    </ConfigProvider> 
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
