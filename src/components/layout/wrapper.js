import React from 'react';
import Sider from './components/Side';
import Header from './components/Header';
import './layout.scss';

class PageWrapper extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userInfo: '',
    }
  }


  render() {
    const domEle = (
      <div className="pageWrapper">
        <div className="headerWrapper">
          <Header></Header>
        </div>
        <div className="downWrapper">
          <Sider></Sider>
          <div className="contentWrapper">
            { this.props.children }
          </div>
        </div>
      </div>
    )
    return domEle;
  }
}

export default PageWrapper;