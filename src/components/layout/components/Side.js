import React from 'react';
import { connect } from 'react-redux';
import { handleChangeNav } from '../../../redux/actions/side';
import { AppstoreOutlined, MailOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { withRouter } from 'react-router';

const { SubMenu } = Menu;

@withRouter
class Sider extends React.Component {

  handleClick(e) {
    if (e.keyPath && e.keyPath.length) {
      const [ selectKey, openKey ] = e.keyPath;
      this.props.handleChangeNav(selectKey, openKey)
      console.log('e.keyPath:', e.keyPath)
      console.log('this.props.navs:', this.props)
      // this.props.history.push(selectKey,'123123')
        this.props.history.push({
          pathname: selectKey,
          state: 'qweqwe',
          search: '?name=123&age=12'
        })
      
    }
  }

  render() {
    
    return (
      <Menu
        className="borderRight"
        onClick={(e) => this.handleClick(e)}
        style={{ width: 256, height: '100vh' }}
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['home']}
        mode="inline"
      >
        <Menu.Item key="/home/demo" icon={<MailOutlined />} >测试</Menu.Item>
        <SubMenu
          key="home"
          icon={<MailOutlined />}
          title="项目一"
        >
          <Menu.ItemGroup key="g1" title="统计">
            <Menu.Item key="/home/line">图表数据</Menu.Item>
            <Menu.Item key="/home/homeChart">测试空文档</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g2" title="sub项目2">
            <Menu.Item key="/home/zCom">sub项目3</Menu.Item>
            <Menu.Item key="/home/display">sub项目4</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>

        <SubMenu
          key="chart"
          icon={<AppstoreOutlined />}
          title="图表"
        >
          <Menu.Item key="line">折线图</Menu.Item>
        </SubMenu>
      </Menu>
    )
  }

 
}

const mapStateToProps = (store) => {
  // console.log('store:', store)
  return store.navs
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//     navChange(selectKey, openKey) {
//       dispatch(handleChangeNav(selectKey, openKey))
//     }
//   }
// }

export default connect(mapStateToProps, {handleChangeNav})(Sider);
