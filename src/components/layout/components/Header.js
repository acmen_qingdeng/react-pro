import React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { BellOutlined, GithubOutlined, SettingOutlined, ExportOutlined } from '@ant-design/icons';
import { Space, Button } from 'antd';
import configInfo from '@/appConfig';
import { setUserInfo, clearUserInfo } from '@/redux/actions/userInfo';
/* 这里使用修饰符，注意项目默认是不支持修饰符的，需要通过webpack配置，这里我配置在craco的babel插件中（详见craco.config.js） */
@withRouter
class Header extends React.Component {

  handleLogoClick() {
    console.log('logo click')
  }

  handleSetInfo() {
    console.log('click to show this info and set user info:', this)
  }

  handleLogout() {
    this.props.removeUserInfo();
    console.log('this info removed:', this.props)
    this.props.history.push({
      pathname: '/login',
      state: {name: 'Tom'}
    })
  }

  render() {
    console.log('this.props', this.props)

    const domEle = (
      <div className="headerWrapper">
        <div className="logoWrapper" onClick={() => this.handleLogoClick()}>
          { configInfo.systemName }
        </div>
        <div className="navWrapper">
          <Space
            size="large"
            align="center"
          >
            <Button className="" size="large" type="text" icon={ <GithubOutlined /> } onClick={() => this.handleSetInfo()}></Button>
            <Button className="" size="large" type="text" icon={ <BellOutlined /> }></Button>
            <Button className="" size="large" type="text" icon={ <SettingOutlined /> }></Button>
            <Button className="" size="large" type="text" icon={ <ExportOutlined />} onClick={() => this.handleLogout() } ></Button>
          </Space>
        </div>
      </div>
    )
    return domEle
  }
}

const mapStateToProps = (state) => {
  return state.userInfo;
}

const mapDispatchToProps = (dispatch) => {
  return {
    storageUserInfo: (info) => {
      dispatch(setUserInfo(info));
    },

    removeUserInfo: () => {
      dispatch(clearUserInfo());
    },
  }
}

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
export default connect(mapStateToProps, mapDispatchToProps)(Header);