import { combineReducers } from 'redux'

import { todosReducer } from './reduces/todo';  // todo 模块
import { visibilityFilter } from './reduces/visibleFilter'; // 其他模块
import { navChangeReducer } from './reduces/side';
import { userInfoReducer } from './reduces/userInfo';

const reducers = combineReducers({
  todos: todosReducer,
  visibilityFilter,
  navs: navChangeReducer,
  userInfo: userInfoReducer,
})

export default reducers
