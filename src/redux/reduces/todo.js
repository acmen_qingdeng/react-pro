
export const todosReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        action.payload,
      ]
    case 'TOGGLE_TODO':
      return state.map(todo =>
        (todo.id === action.id) 
          ? {...todo, completed: !todo.completed}
          : todo
      )
    case 'DELETE_TODO': 
      let arr = [];
       state.forEach(todo =>{
          if(todo.id !== action.id)  {
            arr.push( todo )
          }
        }
      )
      return arr
    default:
      return state
  }
}