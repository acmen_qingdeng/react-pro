import { SET_USER_INFO, CLEAR_USER_INFO } from '../actions/userInfo';


export const userInfoReducer = (state={}, action) => {
  switch(action.type) {
    case SET_USER_INFO: 
     return Object.assign({}, state, action.info);
      
    case CLEAR_USER_INFO: 
      return {}

    default: 
      return state
  }
}