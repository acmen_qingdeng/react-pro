export const navChangeReducer = (state = {selectKey: '1',openKey: 'home'}, action) => {

  switch (action.type) {
    case 'CHANGE_MENU':
      return {
        selectKey: action.selectKey,
        openKey: action.openKey
      }
    case 'TOGGLE_TODO':
      return state.map(todo =>
        (todo.id === action.id) 
          ? {...todo, completed: !todo.completed}
          : todo
      )
    case 'DELETE_TODO': 
      let arr = [];
       state.forEach(todo =>{
          if(todo.id !== action.id)  {
            arr.push( todo )
          }
        }
      )
      return arr
    default:
      return state
  }
}