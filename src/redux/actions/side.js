const CHANGE_MENU = 'CHANGE_MENU';

export const handleChangeNav = (selectKey, openKey) => ({
  type: CHANGE_MENU,
  selectKey: selectKey,
  openKey: openKey
})

