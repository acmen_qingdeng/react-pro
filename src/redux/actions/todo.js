const ADD_TODO = 'ADD_TODO';
const TOGGLE_TODO = 'TOGGLE_TODO';
const DELETE_TODO = 'DELETE_TODO';


export const addTodo = (content) => ({
    type: ADD_TODO,
    payload: {
      id: new Date().getTime(),
      content,
      completed: false,
      createdAt: new Date().getTime()
    },
    des: '添加待办'
})

export const toggleTodo = (todoId) => ({
  type: TOGGLE_TODO,
  id: todoId,
  des: '切换代办状态',
})

export const delTodo = (todoId) => ({
  type: DELETE_TODO,
  id: todoId,
  des: '删除代办'
})