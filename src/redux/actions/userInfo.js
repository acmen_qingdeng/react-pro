export const SET_USER_INFO = 'SET_USER_INFO';
export const GET_USER_INFO = 'GET_USER_INFO';
export const CLEAR_USER_INFO = 'CLEAR_USER_INFO';

export const setUserInfo = (info) => {
  return {
    type: SET_USER_INFO,
    info,
    des: '添加/修改/覆写用户信息'
  }
}

// export const getUserInfo = (info) => {
//   return {
//     type: SET_USER_INFO,
//     info
//   }
// }

export const clearUserInfo = () => {
  return {
    type: CLEAR_USER_INFO,
    tips: '清空信息'
  }
}