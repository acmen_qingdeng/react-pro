import React from 'react';
import '../login/varible.scss'

class App extends React.Component {

  render() {
    return (
      <div className="appWrapper">
        <p>这里是父容器</p>
        {this.props.children}
      </div>
    )
  }
}

export default App;