import React from 'react';
// import { Router, Route, Redirect } from 'react-router';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import { createBrowserHistory } from 'history';

import { zzh_Module } from './routes';

export default class RouterView extends React.Component {

  generateModuleRoutes(routesList) {
    if (routesList && routesList.length) {
      return (
        routesList.map((item, index) => (
          <Route path={item.path} component={item.component} key={`${index}-${item.path}}`} ></Route>
        ))
      )
    } else {
      console.error('模块路由不能为空')
      return null
    }
  }

  render() {
    return (
      <Router history={createBrowserHistory()} >
        
        <Switch>
          {
            this.generateModuleRoutes(zzh_Module)
          }
          <Redirect from="/" to="/login"></Redirect>
        </Switch>
        
      </Router>
    )
  }

}


