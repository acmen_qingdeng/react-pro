/* 
 * @author zzh
 * @email 1454758148@qq.com
 * @date 2021-09-23
 * @desc 注意，当前抽取出来的js文件，是一个扁平化的一维路由列表（不包含children）；
 *   通过./index.js文件中generateModuleRoutes方法生成路由dom；
 */
import Login from '../views/login/Login';
import Clock from '../views/login/clock/Clock';
import Button from '../views/login/button/Button';
import Up from '../views/login/up/Up';
import Table from '../views/table/table';
import HomePage from '../views/home/Home';
import Display from '../views/display/index';
import Zcom from '../views/zCom/index';
import homeChart from '../views/homeChart/index';
import HomeChart from '../views/homeChart/index';

/* 模块一： */
export const zzh_Module = [
  {
    path: '/login',
    component: Login,
    meta: {
      exact: true
    }
  },
  {
    path: '/t/clock',
    component: Clock,
    meta: {}
  },
  {
    path: '/t/button',
    component: Button,
    meta: {}
  },
  {
    path: '/t/up',
    component: Up,
    meta: {}
  },
  {
    path: '/table',
    component: Table,
    meta: {}
  },
  {
    path: '/home/line',
    component: HomePage,
    meta: {}
  },
  {
    path: '/home/homeChart',
    component: HomeChart,
    meta: {}
  },
  {
    path: '/home/zCom',
    component: Zcom,
    meta: {}
  },
  {
    path: '/home/display',
    component: Display,
    meta: {}
  },
]

/* 模块二： */
// 。。。